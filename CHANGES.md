# v0.99 Changes

* fixed: menu line restore after i/o error didn't work in v0.50.
* fixed: to enter a cluster# in "goto cluster" did jump to a wrong
* cluster if cluster number contained a '9'.
* fixed: Alt-Left at cluster 2 caused an error with a strange cluster#.
* fixed: unformat FAT12/FAT16 didn't work in v0.50
* fixed: unformat works for FAT32.
* fixed: unformat may have stopped prematurely, leaving the disk in a corrupted state.
* fixed: in v0.50, WDe had problems to handle image files correctly.
* fixed: when restoring a file chain and the source file was a bit smaller than what's stored in the directory entry, WDe did show that last sector after the copy process.
* fixed: function "find MBR" wasn't correct - did check 3 entries only, and assumed word at offset 1FCh to be 0000.
* fixed: find MBR/BS/FAT/Dir displayed a wrong sector if the item was "found" in the current sector.
* in directory view, files and directory names are displayed in different colors, the rest is darkened a bit ( light grey ).
* filenames now accepted as cmdline parameters.
* -m cmdline option may now be entered without number.
* added option -8 to setm43.exe and setm432.exe.*
* debug version writes to debug terminal when running in a NTVDM.
